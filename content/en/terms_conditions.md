---
##################################################
# These fields should be completed for each file #
##################################################
title: "General Terms and Conditions of Delivery" #Max 70 characters
slug: "gtc" #the content slug url
breadcrumb: "General Terms and Conditions of Delivery"
description: "General Terms and Conditions of Delivery" #Max 160 characters
keywords: datenschutz #Ignored by Google
translationKey: "agb"
#tags: ["tag1","tag2","tag3"] #For Taxonomy Templates
#categories: ["cat1"] #For Taxonomy Templates
draft: false #or true to not render
#language: "de" #Identification for NetlifyCMS
type: "page" #Template folder in /layouts/
layout: "single" #Template file

############################################
# There are different types of schema      #
# e.g. WebPage, Article, Book, Recipe...   #
# Some of them are already created in the  #
# file /layouts/partials/meta_schema.html. #
# Info: https://schema.org/docs/full.html  #
############################################
schemaType: "" #Default: LocalBusiness

#################################
# If these fields are empty     #
# the information will be taken #
# from /data/siteVar.yaml       #
#################################
publisher: "" #Agency or company
copyright: "" #Your name or company
author: "" #Person who wrote this text/article
pageType: #Type of the page e.g. description, help
  - datenschutz
pageTopic: #Categories e.g. business, science, ...
  - informationen
audience: "" #Target group e.g. designer, all
publishDate: "" #First release of this page e.g. 2020-01-19T15:01:05+01:00
lastModDate: "" #Last modification date of this page e.g. 2020-01-21T15:01:05+01:00
robots: "" #index, follow, none...
revisitAfter: "" #Bot revisit page e.g. 14 days
openGraphImage: "" #e.g. 'image.jpg' #1200x630px #Located in same directory #Use File-Extension .jpg or .png
formWithRecaptcha: "false" #set to true if you need the GoogleRecaptcha; loads the script file in partials/inline_javascript.html
---

<p>
	<h2>1. Offer, conclusion and contents of contract</h2>
	<br />
	1.1. Our terms and conditions of sale shall apply exclusively. They are accepted by the Customer when the order is placed, at the latest when the first delivery/service is acknowledged, and shall apply for the duration of the business relationship.<br />
	<br />
	We do not recognise any terms and conditions of the Customer that are contrary to or deviate from our terms and conditions of sale unless we have expressly agreed to their validity in writing. Our terms and conditions of sale shall apply even if we execute a delivery to the Customer without reservation in the knowledge that the customer's terms and conditions are contrary to or deviate from our terms and conditions of sale.<br />
	<br />
	All agreements made between us and the Customer for the purpose of executing this contract are stated in writing in this contract.<br />
	<br />
	These terms and conditions of sale shall apply only to companies within the meaning of § 310 (1) BGB (German Civil Code).<br />
	<br />
	1.2. Our offer shall be subject to change, unless otherwise stated in the order confirmation. The contract shall only originate upon our written confirmation and in accordance with its contents and through performance/delivery. If delivery/performance is effected immediately without a confirmation, the invoice shall simultaneously count as order confirmation.<br />
	<br />
	We reserve the property rights and copyrights to illustrations, visualisations, drawings, plans, calculations and other documents. This also applies to such written documents that are designated as "confidential". The Customer requires our express written consent before forwarding them to third parties.<br />
	<br />
	1.3. Costs for production of drawings for special constructions shall be paid by the Customer, insofar as the offer does not lead to an order for reasons for which we are not responsible.<br />
	<br />
	1.4. All information on weights, dimensions, performance and technical data included in our printed matter, catalogues, price lists or other contractual documents are only approximations unless they are expressly stated as binding.<br />
	<br />
	1.5. We reserve the right to effect changes to the construction and form of the subject matter of the contract, insofar as these do not result in changes that are unreasonable for the Customer.<br />
	<br />
	1.6. In respect of tests where certain temperatures, times and other measurement or control values are to apply, the corresponding measurement methods must be defined and recognised by both parties before the start of delivery. If no specification is made, our measuring methods shall apply.<br />
	<br />
	1.7. Samples shall only be delivered against payment.<br />
	<br />
	1.8. When orders have been placed, they shall be irrevocable unless the supplier has agreed to the cancellation in writing. Orders placed that are binding can also be changed by the Customer only with our consent; in such a case the Customer must reimburse us for all additional costs arising from the change to the order. The Customer shall be charged EUR 78.00 net in respect of administrative expenses incurred in this way. The costs for work already carried out or materials used must likewise be reimbursed by the Customer. In the case of an order cancellation approved by us, we shall be entitled to payment of the agreed remuneration less any expenses saved by us.<br />
	<br />
	1.9. In any case, goods may be returned only with express consent from the supplier. The return must be carriage paid, stating the order number and delivery date in the original delivery packaging. The goods must be in their original condition, i.e. undamaged. We shall charge 20% of the value of the goods, but at least € 50.00 plus VAT at the statutory rate, for processing costs in respect of the return. The supplier reserves the right to charge the Customer a higher expense in individual cases against proof. The customer's rights in relation to defects in accordance with section 7 shall remain unaffected.<br />
	<br />
	1.10. In the case of export transactions, delivery shall be made based on terms and conditions agreed on the order confirmation that are to be understood in accordance with the international rules for the interpretation of customary forms of contract (incoterms 2010) of the International Chamber of Commerce, as applicable on the date of conclusion of the contract, unless any different interpretation has been agreed.<br />
	<br />
	<h2>2. Prices</h2>
	<br />
	2.1. Unless otherwise stated in the order confirmation, our prices shall be "ex works", excluding packaging; the latter shall be invoiced separately. Statutory value-added tax is not included in our prices; it is shown separately on the invoice at the statutory rate on the date of invoicing. In the case of export transactions, the delivery item is deemed to be sold "ex works" if the contract does not state the type of sale. For individual orders with a net goods value below € 100.00 a processing fee of € 10.00 plus the statutory value added tax shall be charged in Germany.<br />
	<br />
	2.2. We draw attention to the fact that we shall only effect the dispatch at the Customer's request. The provisions of section 5 shall remain unaffected.<br />
	<br />
	2.3. We shall charge the prices valid at the time of conclusion of the contract that are based on the cost factors valid at that time. We reserve the right to amend our prices accordingly if cost increases arise after the conclusion of the contract, in particular due to collective agreements or changes in prices of materials. We shall be required to proceed in the same way in case of cost reductions. Upon request, we shall provide the Customer with evidence of cost reductions and cost increases as soon as and insofar as they arise, and shall take them into account in the case of cost increases and cost reductions.<br />
	<br />
	In the case of export transactions, the supplier shall have the right, in the event of a devaluation of the currency in which the order is placed, to cancel the part of the order not yet concluded or to adjust the prices for it accordingly.<br />
	<br />
	2.4. If the contract is concluded "ex works", the goods shall be transported at the expense and risk of the Customer. The provisions set out in the incoterms 2010 shall apply to all consignments in respect of insurance and assumption of risk. If the insurance is covered by the seller based on the agreement concluded, the Customer shall be entitled to compensation from the insurance company in the case of loss or damage to the extent to which the seller himself obtains satisfaction from the insurance company. The supplier shall insure his transports at least at the level of the invoice value.<br />
	<br />
	<h2>3. Payment terms</h2>
	<br />
	3.1. In the case of customers who are not sufficiently known to us, or if we are not satisfied with the credit circumstances of the Customer, the goods shall be dispatched against cash on delivery or against advance payment.<br />
	<br />
	If the Customer is in default with a payment or if there are concrete indications of an imminent insolvency on the part of the Customer, e.g. in the form of a bad credit report, we may demand immediate advance payment of all our claims, including those not yet due, including bills of exchange and deferred amounts, or corresponding collateral. If the Customer fails to comply with our request for advance payment or provision of collateral within a reasonable period of time, we shall be entitled to withdraw from the contract (or the contracts) and charge the Customer for the costs incurred up to that point, including lost profit.<br />
	<br />
	3.2. We accept discountable and properly taxed bills of exchange on account of payment if this has been expressly agreed in writing beforehand. Credit notes for bills of exchange and cheques shall be subject to receipt less expenses with value date of the day on which we have disposal of the equivalent value.<br />
	<br />
	3.3. Unless otherwise stated in the order confirmation, the purchase price shall be due for payment net (without deduction) within 30 days of the invoice date.<br />
	<br />
	3.4. The statutory regulations concerning the consequences of default in payment shall apply.<br />
	<br />
	3.5. The Customer shall be entitled to offsetting rights only if his counterclaims are legally effective, are undisputed or have been acknowledged by us. Furthermore, the Customer shall be entitled to exercise a right of retention insofar as his counterclaim is based on the same contractual relationship.<br />
	<br />
	3.6. For export transactions, payments are to be made in accordance with the agreed terms of payment.<br />
	<br />
	<h2>4. Delivery time</h2>
	<br />
	4.1. Stated delivery times shall be binding only if a delivery time has been definitely confirmed by us or can be determined according to the calendar in the contract.<br />
	<br />
	4.2. Compliance with our delivery obligation shall require the punctual and proper fulfilment of the customer's obligations. We reserve the right to assert the defence of non-performance of the contract. The start of the delivery time stated by us shall also require clarification of all technical questions.<br />
	<br />
	4.3. If a delivery date is agreed, the supplier must deliver punctually. The delivery period shall be deemed to have been complied with if the delivery item has left the factory or readiness for dispatch has been notified by the end of the period; this shall be subject to our receipt of timely and correct supply. If the Customer changes parts of the delivery, the delivery period shall not re-start until the change has been confirmed.<br />
	<br />
	We shall be liable in accordance with the statutory provisions insofar as the underlying purchase contract is an agreement for a transaction on a fixed date within the meaning of § 286 (2) no. 4 BGB or § 376 HGB (German Commercial Code). We shall also be liable in accordance with the statutory provisions if, as a result of a delay in delivery for which we are responsible, the Customer is entitled to assert that his interest in further fulfilment of the contract has ceased.<br />
	<br />
	We shall likewise be liable in accordance with the statutory provisions if the delay in delivery is due to an intentional or grossly negligent breach of contract for which we are responsible; any culpability on the part of our representatives or vicarious agents shall be attributed to us. Insofar as the delay in delivery is due to a grossly negligent breach of contract for which we are responsible, our liability for damages shall be limited to the foreseeable loss or damage that typically occurs.<br />
	<br />
	We shall also be liable in accordance with the statutory provisions if the delay in delivery for which we are responsible is based on the culpable breach of a material contractual obligation; however, in this case liability for loss or damage shall be limited to the foreseeable loss or damage that typically occurs.<br />
	<br />
	Further legal claims and rights of the Customer remain reserved.<br />
	<br />
	4.4. Force majeure, war, riot, strike, lockout or measures taken by authorities, irrespective of the reason, that prevent a delivery, as well as lack of raw materials, means of transport and theft - even at upstream suppliers' premises - shall release the supplier from the obligation to affect delivery within the agreed period. The Customer shall be notified immediately of the occurrence of the event and its expected effects. With the cessation of the obligation to perform, the obligation to render a consideration in return shall also cease.<br />
	<br />
	4.5. Deliveries before expiry of the delivery time shall be permissible.<br />
	<br />
	4.6. In the case of delayed delivery or impossibility, the regulations of Section 9 shall apply.<br />
	<br />
	<h2>5. Transfer of risk and acceptance</h2>
	<br />
	5.1. Unless otherwise stated in the order confirmation, delivery "ex works" is agreed.<br />
	<br />
	5.2. If the Customer so wishes, we shall cover the delivery with transport insurance; the costs incurred in this way shall be paid by the Customer.<br />
	<br />
	5.3. Separate agreements shall apply to taking back the packaging.<br />
	<br />
	5.4. If the Customer is in default in acceptance or culpably violates other obligations of cooperation, the risk of accidental loss or accidental deterioration of the purchased object shall pass to the Customer at the point in time when he is in default of acceptance or debtor's default.<br />
	<br />
	5.5. Delivered items, even if they have minor defects, are to be accepted by the Customer notwithstanding the rights in Section 8.<br />
	<br />
	<h2>6. Default in acceptance, order on call, retention of title, assignment of claims</h2>
	<br />
	6.1. If the Customer is in default in acceptance or if he culpably violates other obligations of cooperation, we shall be entitled to demand compensation for the loss or damage incurred by us in this respect, including any additional expenses. Further claims or rights are reserved.<br />
	<br />
	6.2. Orders that are confirmed by us on call - unless otherwise agreed - must be taken within one year of the date of order at the latest. The same shall apply in the case of postponement of dates or sustained "on/off call". In the event of non-call within the stated period, point 6.1. shall apply accordingly.<br />
	<br />
	6.3.<br />
	<br />
	(1) We reserve the right of ownership of the object of sale until all payments from the delivery contract are received. If the Customer acts in breach of contract, in particular in the case of default in payment, we shall be entitled to recover the object of sale. Our recovery of the object of sale shall constitute a withdrawal from the contract. After recovering the object of sale, we shall be entitled to sell it; the proceeds of the sale shall be set off against the customer's liabilities - less reasonable costs of sale.<br />
	<br />
	(2) The Customer shall be required to handle the purchased item with care; in particular, he must insure it sufficiently at his own expense against fire, water and theft damage at the replacement value. If maintenance and inspection work is required, the Customer must carry this out in good time at his own expense.<br />
	<br />
	(3) In the event of seizure or other interventions by third parties, the Customer must inform us immediately in writing to enable us to take legal action in accordance with § 771 ZPO (Code of Civil Procedure). Insofar as the third party is not in a position to reimburse us for the court and out-of-court costs of an action in accordance with § 771 ZPO, the Customer shall be liable for the loss that we incur.<br />
	<br />
	(4) The Customer shall be entitled to resell the object of sale in the course of normal business; however, he hereby assigns to us already now all claims at the level of the final invoice amount (including VAT) of our claim that accrue to him against his customers or third parties from the resale, irrespective of whether the object of sale has been resold without processing or after processing. If there is a current account relationship between the Customer and the purchaser in accordance with § 355 HGB, the claim assigned to us by the Customer in advance also relates to the acknowledged balance and, in the case of the purchaser's insolvency, to the then existing causal balance. The Customer shall remain authorised to collect this claim even after the assignment. Our entitlements to collect the claim ourselves shall remain unaffected by this. We undertake, however, to refrain from collecting claim as long as the Customer fulfils his payment obligations from the collected proceeds, does not default in payment and in particular no application for the opening of composition or insolvency proceedings has been filed and payments have not been suspended. However, if this is the case, we may demand that the Customer informs us of the assigned claims and their debtors, provides all information required for collection, hands over the relevant documentation and notifies the debtors (third parties) of the assignment.<br />
	<br />
	(5) The processing or alteration of the purchased goods by the Customer is always carried out for us. If the object of sale is processed with other objects that do not belong to us, we shall acquire co-ownership of the new object in the ratio of the value of the object of sale (final invoice amount including VAT) to the other processed objects at the time of processing. In all other respects, the same shall apply to the object created by processing as to the object of sale delivered under reservation.<br />
	<br />
	(6) If the object of sale is combined inextricably with other objects that do not belong to us, we shall acquire co-ownership of the new object in the ratio of the value of the object of sale (final invoice amount including VAT) to the other combined objects at the time of combination. If the combining is carried out in such a way that the Customer's item is to be regarded as the main item, it is agreed that the Customer transfers proportional co-ownership to us. The Customer shall hold the sole ownership or co-ownership that arises in this way for us.<br />
	<br />
	(7) The Customer also assigns to us the claims to secure our claims against him that arise against a third party through the connection of the object of sale with an item of real property.<br />
	<br />
	(8) We undertake to release the collateral to which we are entitled at the Customer's request to the extent that the realisable value of our collateral exceeds the value of the claims to be secured by more than 10%; selection of the collateral to be released shall be at our discretion.<br />
	<br />
	<h2>7. Notice of defects</h2>
	<br />
	Claims for defects by the Customer requires the Customer to have complied correctly with his obligations to examine and notify defects pursuant to § 377 HGB.<br />
	<br />
	<h2>8. Warranty and liability</h2>
	<br />
	8.1. The quality of the goods is based exclusively on the agreed technical delivery specifications. If we are required to deliver according to drawings, specifications, samples, etc. of a partner, the latter shall assume the risk of suitability for the intended purpose. The decisive factor for the condition of the goods in compliance with the contract is the time of the transfer of risk in accordance with section 5.1. In addition, with regard to textiles we refer to a) the "Guideline for the assessment of ready-made awning covers", as of December 2016, of the ITRS (Industrieverband Technische Textilien - Rollladen- Sonnenschutz e.V.), and b) the "Guideline for the cleaning and care of awning covers", as of June 2010, of the ITRS. For product components consisting of aluminium, the "Guideline for the Assessment of Product Properties of Awnings", as of February 2018 of the ITRS shall apply which is also an element of our contractual relationship. These guidelines are available free of charge at https://itrs-ev.com &gt; Publications.<br />
	<br />
	8.2. Insofar as the purchased item is defective, we are shall be entitled to choose between subsequent performance in the form of rectification of the defect or delivery of a new item free of defects. In the case of subsequent performance, we shall bear the necessary expenses only up to the level of the purchase price.<br />
	<br />
	8.3. If the subsequent performance fails, the Customer shall be entitled to demand rescission or reduction of the purchase price at his discretion.<br />
	<br />
	8.4. We shall be liable according to the statutory provisions insofar as the extent that Customer makes claims for loss or damage based on malign intention or gross negligence, including malign intention or gross negligence of our representatives or vicarious agents. Insofar as we are not accused of intentional breach of contract, liability for loss or damage shall be limited to foreseeable loss or damage that typically occurs.<br />
	<br />
	8.5. We shall be liable in accordance with the statutory provisions if we violate a material contractual obligation in a comfortable manner; however, even in this case, liability for loss or damage shall be limited to the foreseeable loss or damage that typically occurs.<br />
	<br />
	8.6. Insofar as the Customer is entitled to compensation for loss or damage in lieu of performance due to a negligent breach of duty, our liability shall be limited to compensation for foreseeable loss or damage that typically occurs.<br />
	<br />
	8.7. Liability on account of culpable damage to life, limb or health shall remain unaffected; this shall also apply to mandatory liability according to the German Product Liability Act.<br />
	<br />
	8.8. Liability shall be excluded, insofar as nothing to the contrary is agreed above.<br />
	<br />
	8.9. The limitation period for claims from defects shall be 24 months, starting from the time of transfer of risk. This shall not apply if the purchased item is customarily used for a building and has caused the defect.<br />
	<br />
	8.10. The period of limitation in the case of a recourse to a manufacturer or supplier according to §§ 478, 479 BGB shall remain unaffected; it shall be five years, calculated from delivery of the defective item.<br />
	<br />
	<h2>9. Liability for subsidiary obligations, other liability</h2>
	<br />
	9.1. Any further liability for loss or damage other than that provided for in Section 8 shall be excluded, irrespective of the legal nature of the claim asserted. This shall apply in particular to claims for damages arising from culpa in contrahendo, other breaches of duty or tortious claims for compensation for property damage in accordance with § 823 BGB.<br />
	<br />
	9.2. The limitation according to section 9.1. shall also shall apply if the Customer demands compensation for wasted expenses instead of a claim for compensation for the loss or damage or performance.<br />
	<br />
	9.3. Insofar as liability for damages against us is excluded or limited, this shall also apply in respect of personal liability for damages of our employees, workers, staff, representatives and vicarious agents.<br />
	<br />
	<h2>10. Industrial property rights, tools</h2>
	<br />
	10.1. The Customer shall assume sole responsibility for the documents, plans, samples or similar items to be provided by him. The Customer shall be responsible for ensuring that the design drawings submitted by him do not infringe industrial property rights of third parties. We shall not have an obligation in relation to the Customer to check whether any industrial property rights of third parties are infringed by the submission of offers. If liability nevertheless arises, the Customer must indemnify us. We shall be liable according to the statutory provisions insofar as the extent that Customer makes claims for loss or damage based on malign intention or gross negligence, including malign intention or gross negligence of our representatives or vicarious agents. Insofar as we are not accused of intentional breach of contract, liability for loss or damage shall be limited to foreseeable loss or damage that typically occurs.<br />
	<br />
	10.2. The operating objects that we have manufactured for the production of the contractual objects on behalf of the Customer, in particular tools, devices, etc., shall remain our property even if they are invoiced separately or the Customer has contributed to their costs, and shall not be delivered even if the contract is terminated.<br />
	<br />
	<h2>11. Place of performance and place of jurisdiction</h2>
	<br />
	11.1. Unless otherwise stated in the order confirmation, our place of business - i.e. the registered office of our company in Betzenweiler - shall be the place of performance.<br />
	<br />
	11.2. If the Customer is a businessperson or enterprise, our registered office shall be the place of jurisdiction; however, we shall also be entitled to bring an action against the Customer at the court that has jurisdiction for his place of residence.<br />
	<br />
	11.3 The law of the Federal Republic of Germany shall apply. The provisions of the UN Sales Convention are expressly excluded.<br />
	<br />
	<h2>12. Reservation of right of modification</h2>
   We expressly declare to the Customer our willingness to formulate the content of contractual clauses by free mutual negotiation.</p>
