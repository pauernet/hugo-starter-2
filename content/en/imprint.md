---
##################################################
# These fields should be completed for each file #
##################################################
title: "Imprint" #Max 70 characters
slug: "imprint" #the content slug url
description: "The important imprint with every info you maybe need" #Max 160 characters
keywords: imprint,contact,address,tax-number #Ignored by Google
translationKey: "impressum"
breadcrumb: "Imprint"
#tags: ["tag1","tag2","tag3"] #For Taxonomy Templates
#categories: ["cat1"] #For Taxonomy Templates
draft: false #or true to not render
#language: "en" #Identification for NetlifyCMS
type: "page" #Template folder in /layouts/
layout: "single" #Template file

############################################
# There are different types of schema      #
# e.g. WebPage, Article, Book, Recipe...   #
# Some of them are already created in the  #
# file /layouts/partials/meta_schema.html. #
# Info: https://schema.org/docs/full.html  #
############################################
schemaType: "" #Default: LocalBusiness

#################################
# If these fields are empty     #
# the information will be taken #
# from /data/siteVar.yaml       #
#################################
publisher: "" #Agency or company
copyright: "" #Your name or company
author: "" #Person who wrote this text/article
pageType: #Type of the page e.g. description, help
  - imprint
  - contact
  - tax-number
pageTopic: #Categories e.g. business, science, ...
  - informationen
audience: "" #Target group e.g. designer, all
publishDate: "" #First release of this page e.g. 2020-01-19T15:01:05+01:00
lastModDate: "" #Last modification date of this page e.g. 2020-01-21T15:01:05+01:00
robots: "" #index, follow, none...
revisitAfter: "" #Bot revisit page e.g. 14 days
openGraphImage: "" #e.g. 'image.jpg' #1200x630px #Located in same directory #Use File-Extension .jpg or .png
formWithRecaptcha: "false" #set to true if you need the GoogleRecaptcha; loads the script file in partials/inline_javascript.html
---

<h2>Information pursuant to § 5 TMG</h2>
<p>Daniel Lassak<br />
Montfortstr. 6<br />
88515 Langenenslingen</p>

<p><strong>Represented by:</strong><br />
Daniel Lassak</p>

<h2>EU dispute resolution</h2>
<p>The European Commission provides a platform for online dispute resolution (ODR): <a href="https://ec.europa.eu/consumers/odr" target="_blank" rel="noopener noreferrer">https://ec.europa.eu/consumers/odr</a>.<br /> Our e-mail address can be found under <a href="/contact" title="contact">contact</a>.</p>

<p>We are not willing or obliged to participate in dispute resolution proceedings before a consumer arbitration board.</p>

<h3>Liability for Contents</h3> <p>As service providers, we are liable for own contents of these websites according to Paragraph 7, Sect. 1 German Telemedia Act (TMG). However, according to Paragraphs 8 to 10 German Telemedia Act (TMG), service providers are not obligated to permanently monitor submitted or stored information or to search for evidences that indicate illegal activities.</p> <p>Legal obligations to removing information or to blocking the use of information remain unchallenged. In this case, liability is only possible at the time of knowledge about a specific violation of law. Illegal contents will be removed immediately at the time we get knowledge of them.</p> <h3>Liability for Links</h3> <p>Our offer includes links to external third party websites. We have no influence on the contents of those websites, therefore we cannot guarantee for those contents. Providers or administrators of linked websites are always responsible for their own contents.</p> <p>The linked websites had been checked for possible violations of law at the time of the establishment of the link. Illegal contents were not detected at the time of the linking. A permanent monitoring of the contents of linked websites cannot be imposed without reasonable indications that there has been a violation of law. Illegal links will be removed immediately at the time we get knowledge of them.</p> <h3>Copyright</h3> <p>Contents and compilations published on these websites by the providers are subject to German copyright laws. Reproduction, editing, distribution as well as the use of any kind outside the scope of the copyright law require a written permission of the author or originator. Downloads and copies of these websites are permitted for private use only.<br /> The commercial use of our contents without permission of the originator is prohibited.</p> <p>Copyright laws of third parties are respected as long as the contents on these websites do not originate from the provider. Contributions of third parties on this site are indicated as such. However, if you notice any violations of copyright law, please inform us. Such contents will be removed immediately.</p>

{{< stockimages >}}
