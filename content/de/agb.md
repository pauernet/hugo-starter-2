---
##################################################
# These fields should be completed for each file #
##################################################
title: "Allgemeine Lieferungs- und Zahlungsbedingungen" #Max 70 characters
slug: "agb" #the content slug url
breadcrumb: "Allgemeine Geschäftsbedingungen"
description: "Allgemeine Geschäfts-, Lieferungs- und Zahlungsbedingungen" #Max 160 characters
keywords: datenschutz #Ignored by Google
translationKey: "agb"
#tags: ["tag1","tag2","tag3"] #For Taxonomy Templates
#categories: ["cat1"] #For Taxonomy Templates
draft: false #or true to not render
#language: "de" #Identification for NetlifyCMS
type: "page" #Template folder in /layouts/
layout: "single" #Template file

############################################
# There are different types of schema      #
# e.g. WebPage, Article, Book, Recipe...   #
# Some of them are already created in the  #
# file /layouts/partials/meta_schema.html. #
# Info: https://schema.org/docs/full.html  #
############################################
schemaType: "" #Default: LocalBusiness

#################################
# If these fields are empty     #
# the information will be taken #
# from /data/siteVar.yaml       #
#################################
publisher: "" #Agency or company
copyright: "" #Your name or company
author: "" #Person who wrote this text/article
pageType: #Type of the page e.g. description, help
  - datenschutz
pageTopic: #Categories e.g. business, science, ...
  - informationen
audience: "" #Target group e.g. designer, all
publishDate: "" #First release of this page e.g. 2020-01-19T15:01:05+01:00
lastModDate: "" #Last modification date of this page e.g. 2020-01-21T15:01:05+01:00
robots: "" #index, follow, none...
revisitAfter: "" #Bot revisit page e.g. 14 days
openGraphImage: "" #e.g. 'image.jpg' #1200x630px #Located in same directory #Use File-Extension .jpg or .png
formWithRecaptcha: "false" #set to true if you need the GoogleRecaptcha; loads the script file in partials/inline_javascript.html
---

<h2>1. Angebot, Vertragsabschluss und Vertragsinhalt</h2>
<p>1.1. Unsere Verkaufsbedingungen gelten ausschließlich. Sie werden vom Besteller mit Auftragserteilung, spätestens mit der Annahme der ersten Lieferung/Leistung anerkannt und gelten für die Dauer der Geschäftsverbindung.<br />
<br />
Entgegenstehende oder von unseren Verkaufsbedingungen abweichende Bedingungen des Kunden erkennen wir nicht an, es sei denn, wir hätten ausdrücklich schriftlich ihrer Geltung zugestimmt. Unsere Verkaufsbedingungen gelten auch dann, wenn wir in Kenntnis entgegenstehender oder von unseren Verkaufsbedingungen abweichender Bedingungen des Kunden die Lieferung an den Kunden vorbehaltlos ausführen.<br />
<br />
Alle Vereinbarungen, die zwischen uns und dem Kunden zwecks Ausführung dieses Vertrages getroffen werden, sind in diesem Vertrag schriftlich niedergelegt.<br />
<br />
Diese Verkaufsbedingungen gelten nur gegenüber Unternehmern im Sinne des § 310 Abs. 1 BGB.<br />
<br />
1.2. Unser Angebot ist freibleibend, sofern sich aus der Auftragsbestätigung nichts anderes ergibt. Der Vertrag kommt erst mit unserer schriftlichen Bestätigung und entsprechend deren Inhalt und durch Leistung/Lieferung zustande. Erfolgt ohne eine Bestätigung unverzüglich Lieferung/Leistung, so gilt die Rechnung gleichzeitig als Auftragsbestätigung.<br />
<br />
An Abbildungen, Visualisierungen, Zeichnungen, Planungen, Kalkulationen und sonstigen Unterlagen behalten wir uns Eigentums- und Urheberrechte vor. Dies gilt auch für solche schriftlichen Unterlagen, die als „vertraulich“ bezeichnet sind. Vor ihrer Weitergabe an Dritte bedarf der Kunde unserer ausdrücklichen schriftlichen Zustimmung.<br />
<br />
1.3. Kosten für die Herstellung von Zeichnungen für Sonderkonstruktionen sind vom Besteller zu tragen, sofern das Angebot aus Gründen, die von uns nicht zu vertreten sind, nicht zu einem Auftrag führt.<br />
<br />
1.4. Alle Angaben über Gewichte, Abmessungen, Leistungen und technische Daten, die in unseren Drucksachen, Katalogen, Preislisten oder in anderen Vertragsunterlagen enthalten sind, sind nur annähernd maßgebend, soweit sie nicht ausdrücklich als verbindlich bezeichnet sind.<br />
<br />
1.5. Wir behalten uns Konstruktions- und Formänderungen des Vertragsgegenstandes vor, sofern dieser dadurch für den Besteller keine unzumutbaren Änderungen erfährt.<br />
<br />
1.6. Für Prüfungen, bei denen bestimmte Temperaturen, Zeiten und sonstige Mess- oder Regelwerte gelten sollen, müssen vor Lieferbeginn die entsprechenden Messmethoden festgelegt und von beiden Seiten anerkannt werden. Wenn keine Festlegung erfolgt, gelten unsere Messmethoden.<br />
<br />
1.7. Muster werden nur gegen Berechnung geliefert.<br />
<br />
1.8. Einmal erteilte Aufträge sind unwiderruflich, es sei denn, dass der Lieferer der Aufhebung schriftlich zugestimmt hat. Verbindlich erteilte Aufträge können vom Besteller auch nur mit unserer Zustimmung geändert werden; der Besteller hat uns in diesem Falle sämtliche durch die Auftragsänderung zusätzlich entstehenden Kosten zu ersetzen. Für den diesbezüglichen Verwaltungsaufwand werden dem Besteller EUR 78,00 netto berechnet. Die Kosten für bereits durchgeführte Arbeiten bzw. verwendetes Material sind ebenfalls vom Besteller zu ersetzen. Im Falle einer von uns genehmigten Auftragsstornierung besteht ein Anspruch unsererseits auf Zahlung der vereinbarten Vergütung abzüglich von uns ersparter Aufwendungen.<br />
<br />
1.9. Die Rückgabe von Waren darf in jedem Fall nur mit ausdrücklicher Zustimmung des Lieferers vorgenommen werden. Die Rückgabe muss frei Haus erfolgen, unter Angabe der Auftragsnummer und Lieferdatums in der Originallieferverpackung. Die Ware hat sich im Originalzustand, also in unbeschädigtem Zustand zu befinden. Für den Bearbeitungsaufwand der Rückgabe berechnen wir 20 % des Warenwertes, mindestens jedoch 50,00 € zzgl. der gesetzlichen Mehrwertsteuer. Dem Lieferer bleibt vorbehalten, gegen Nachweis im Einzelfall einen höheren Aufwand dem Besteller zu berechnen.<br />
Die Mängelrechte des Kunden nach Abschnitt 7 bleiben hiervon unberührt.<br />
<br />
1.10. Bei Exportgeschäften erfolgt die Lieferung zu den auf der Auftragsbestätigung vereinbarten Bedingungen, die gemäß den internationalen Regeln für die Auslegung handelsüblicher Vertragsformen (incoterms 2010) der internationalen Handelskammer, wie sie am Tage des Vertragsabschlusses gelten, zu verstehen sind, sofern keine andere Auslegung vereinbart ist.<br /></p>
<h2>2. Preise</h2>
<p>2.1. Sofern sich aus der Auftragsbestätigung nichts anderes ergibt, gelten unsere Preise „ab Werk“, ausschließlich Verpackung; diese wird gesondert in Rechnung gestellt. Die gesetzliche Mehrwertsteuer ist nicht in unseren Preisen eingeschlossen; sie wird in gesetzlicher Höhe am Tag der Rechnungsstellung in der Rechnung gesondert ausgewiesen.<br />
Bei Exportgeschäften gilt der Liefergegenstand als „ab Werk" verkauft, falls der Vertrag nichts über die Art des Verkaufs bestimmt. Für Einzelbestellungen unter 100,00 € netto Warenwert wird im Inland eine Bearbeitungsgebühr von 10,00 € zuzüglich der gesetzlichen Mehrwertsteuer berechnet.<br />
<br />
2.2. Wir weisen darauf hin, dass wir die Versendung nur auf Wunsch des Kunden durchführen. Hiervon unbeschadet bleiben die Regelungen gemäß Abschnitt 5.<br />
<br />
2.3. Wir berechnen die bei Vertragsabschluss gültigen Preise, die auf den zu dieser Zeit gültigen Kostenfaktoren basieren.<br />
Wir behalten uns das Recht vor, unsere Preise entsprechend zu ändern, wenn nach Abschluss des Vertrages Kostenerhöhungen, insbesondere aufgrund von Tarifabschlüssen oder Materialpreisänderungen eintreten. In gleicher Weise sind wir verpflichtet, bei Kostensenkungen zu verfahren. Sowohl Kostensenkungen als auch Kostenerhöhungen werden wir, sobald und soweit sie eingetreten sind, dem Kunden auf Verlangen nachweisen und bei Kostenerhöhungen sowie bei Kostensenkungen berücksichtigen.<br />
<br />
Bei Exportgeschäften hat der Lieferer das Recht, im Falle einer Abwertung der Währung, in welcher der Auftrag abgeschlossen ist, den noch nicht abgeschlossenen Teil des Auftrags zu annullieren oder die Preise dafür dementsprechend richtigzustellen.<br />
<br />
2.4. Bei Abschluss „ab Werk“ reist die Ware auf Kosten und Gefahr des Bestellers. Bei allen Sendungen finden in Bezug auf Versicherung und Gefahrentragung die in den incoterms 2010 festgelegten Bestimmungen Anwendung. Wird aufgrund der getroffenen Vereinbarung die Versicherung vom Verkäufer gedeckt, so hat der Besteller in Schadensfällen einen Anspruch auf Entschädigung von Seiten der Versicherungsgesellschaft in der Höhe, in welcher der Lieferer selbst von der Versicherungsgesellschaft befriedigt wird. Der Lieferer versichert seine Transporte mindestens in Höhe des Rechnungswertes.<br /></p>
<h2>3. Zahlungsbedingungen</h2>
<p>3.1. Bei Kunden, die uns nicht genügend bekannt sind, oder wenn uns die Kreditverhältnisse des Bestellers nicht befriedigend erscheinen, erfolgt der Versand gegen Nachnahme oder gegen Vorkasse.<br />
<br />
Gerät der Besteller mit einer Zahlung in Verzug oder liegen konkrete Anhaltspunkte für eine bevorstehende Zahlungsunfähigkeit des Bestellers vor, z.B. in Form einer schlechten Kreditauskunft, so können wir sofortige Vorauszahlung aller unserer auch noch nicht fälligen Forderungen einschließlich Wechsel und gestundeter Beträge oder entsprechende Sicherheitsleistungen verlangen. Kommt der Besteller unserem Verlangen auf Vorauszahlung oder Sicherheitsleistung innerhalb angemessener Frist nicht nach, sind wir berechtigt, vom Vertrag (bzw. von den Verträgen) zurückzutreten und dem Besteller die bis dahin entstandenen Kosten einschließlich entgangenem Gewinn in Rechnung zu stellen.<br />
<br />
3.2. Wir nehmen diskontfähige und ordnungsgemäß versteuerte Wechsel zahlungshalber an, wenn dies vorher ausdrücklich schriftlich vereinbart wurde. Gutschriften über Wechsel und Schecks erfolgen vorbehaltlich des Eingangs abzüglich der Auslagen mit Wertstellung des Tages, an dem wir über den Gegenwert verfügen können.<br />
<br />
3.3. Sofern sich aus der Auftragsbestätigung nichts anderes ergibt, ist der Kaufpreis netto (ohne Abzug) innerhalb von 30 Tagen ab Rechnungsdatum zur Zahlung fällig.<br />
<br />
3.4. Es gelten die gesetzlichen Regeln betreffend die Folgen des Zahlungsverzugs.<br />
<br />
3.5. Aufrechnungsrechte stehen dem Kunden nur zu, wenn seine Gegenansprüche rechtskräftig festgestellt, unbestritten oder von uns anerkannt sind. Außerdem ist er zur Ausübung eines Zurückbehaltungsrechts insoweit befugt, als sein Gegenanspruch auf dem gleichen Vertragsverhältnis beruht.<br />
<br />
3.6. Bei Exportgeschäften sind die Zahlungen entsprechend den vereinbarten Zahlungsbedingungen zu leisten.</p>
<h2> 4. Lieferzeit</h2>
<p>4.1. Angegebene Lieferzeiten sind nur dann verbindlich, wenn eine Lieferzeit fest von uns bestätigt ist oder nach dem Vertrag kalendarisch bestimmbar ist.<br />
<br />
4.2. Die Einhaltung unserer Lieferverpflichtung setzt die rechtzeitige und ordnungsgemäße Erfüllung der Verpflichtung des Kunden voraus. Die Einrede des nicht erfüllten Vertrages bleibt vorbehalten.<br />
Der Beginn der von uns angegebenen Lieferzeit setzt weiter die Abklärung aller technischen Fragen voraus.<br />
<br />
4.3. Ist ein fester Liefertermin vereinbart, so hat der Lieferer auch fristgemäß zu liefern. Die Lieferfrist ist eingehalten, wenn bis zu ihrem Ablauf der Liefergegenstand das Werk verlassen hat oder die Versandbereitschaft mitgeteilt ist; rechtzeitige und richtige Selbstbelieferung bleibt vorbehalten. Ändert der Besteller Teile der Lieferung, so beginnt die Lieferfrist erst mit der Bestätigung der Änderung neu zu laufen.<br />
<br />
Wir haften nach den gesetzlichen Bestimmungen, soweit der zugrundeliegende Kaufvertrag ein Fixgeschäft im Sinn von § 286 Abs. 2 Nr. 4 BGB oder von § 376 HGB ist. Wir haften auch nach den gesetzlichen Bestimmungen, sofern als Folge eines von uns zu vertretenden Lieferverzugs der Kunde berechtigt ist geltend zu machen, dass sein Interesse an der weiteren Vertragserfüllung in Fortfall geraten ist.<br />
<br />
Wir haften ferner nach den gesetzlichen Bestimmungen, sofern der Lieferverzug auf einer von uns zu vertretenden vorsätzlichen oder grob fahrlässigen Vertragsverletzung beruht; ein Verschulden unserer Vertreter oder Erfüllungsgehilfen ist uns zuzurechnen. Sofern der Lieferverzug auf einer von uns zu vertretenden grob fahrlässigen Vertragsverletzung beruht, ist unsere Schadensersatzhaftung auf den vorhersehbaren, typischerweise eintretenden Schaden begrenzt.<br />
<br />
Wir haften auch dann nach den gesetzlichen Bestimmungen, soweit der von uns zu vertretende Lieferverzug auf der schuldhaften Verletzung einer wesentlichen Vertragspflicht beruht; in diesem Fall ist aber die Schadensersatzhaftung auf den vorhersehbaren, typischerweise eintretenden Schaden begrenzt.<br />
<br />
Weitere gesetzliche Ansprüche und Rechte des Kunden bleiben vorbehalten.<br />
<br />
4.4. Höhere Gewalt, Krieg, Aufruhr, Streik, Aussperrung oder Maßnahmen von Behörden, gleichgültig aus welchem Grund, die einer Lieferung entgegenstehen, sowie Mangel an Rohstoffen, an Transportmitteln sowie Diebstahl - auch bei den Vorlieferanten - entbinden den Lieferer von der Verpflichtung, innerhalb der vereinbarten Frist zu liefern. Von dem Eintreten des Ereignisses und von den voraussichtlichen Auswirkungen ist der Besteller unverzüglich zu benachrichtigen.<br />
Mit dem Wegfall der Leistungspflicht entfällt auch die Gegenleistungspflicht.<br />
<br />
4.5. Lieferungen vor Ablauf der Lieferzeit sind zulässig.<br />
<br />
4.6. Im Falle des Lieferverzugs oder Unmöglichkeit gelten die Regelungen der Ziffer 9.</p>
<h2>5. Gefahrenübergang und Entgegennahme</h2>
<p>5.1. Sofern sich aus der Auftragsbestätigung nichts anderes ergibt, ist Lieferung „ab Werk“ vereinbart.<br />
<br />
5.2. Sofern der Kunde es wünscht, werden wir die Lieferung durch eine Transportversicherung eindecken; die insoweit anfallenden Kosten trägt der Kunde.<br />
<br />
5.3. Für die Rücknahme von Verpackungen gelten gesonderte Vereinbarungen.<br />
<br />
5.4. Kommt der Kunde in Annahmeverzug oder verletzt er schuldhaft sonstige Mitwirkungspflichten, geht die Gefahr eines zufälligen Untergangs oder einer zufälligen Verschlechterung der Kaufsache in dem Zeitpunkt auf den Kunden über, in dem dieser in Annahme- oder Schuldnerverzug geraten ist.<br />
<br />
5.5. Angelieferte Gegenstände sind, auch wenn sie unwesentliche Mängel aufweisen, vom Besteller unbeschadet der Rechte aus Abschnitt 8 entgegenzunehmen.</p>
<h2>6. Annahmeverzug, Bestellung auf Abruf, Eigentumsvorbehalt, Forderungsabtretung</h2>
<p>6.1. Kommt der Kunde in Annahmeverzug oder verletzt er schuldhaft sonstige Mitwirkungspflichten, so sind wir berechtigt, den uns insoweit entstehenden Schaden, einschließlich etwaiger Mehraufwendungen ersetzt zu verlangen. Weitergehende Ansprüche oder Rechte bleiben vorbehalten.<br />
<br />
6.2. Bestellungen, die von uns auf Abruf bestätigt werden - sofern nichts Besonderes vereinbart ist - müssen spätestens innerhalb eines Jahres ab Bestelldatum abgenommen werden. Dasselbe gilt bei Terminrückstellungen oder nachhaltiger "Auf-/ Abrufstellung". Bei Nichtabruf innerhalb der genannten Frist gilt Ziffer 6.1. entsprechend.<br />
<br />
6.3.<br />
<br />
(1) Wir behalten uns das Eigentum an der Kaufsache bis zum Eingang aller Zahlungen aus dem Liefervertrag vor. Bei vertragswidrigem Verhalten des Kunden, insbesondere bei Zahlungsverzug, sind wir berechtigt, die Kaufsache zurückzunehmen. In der Zurücknahme der Kaufsache durch uns liegt ein Rücktritt vom Vertrag. Wir sind nach Rücknahme der Kaufsache zu deren Verwertung befugt, der Verwertungserlös ist auf die Verbindlichkeiten des Kunden – abzüglich angemessener Verwertungskosten – anzurechnen.<br />
<br />
(2) Der Kunde ist verpflichtet, die Kaufsache pfleglich zu behandeln; insbesondere ist er verpflichtet, diese auf eigene Kosten gegen Feuer-, Wasser- und Diebstahlsschäden ausreichend zum Neuwert zu versichern. Sofern Wartungs- und Inspektionsarbeiten erforderlich sind, muss der Kunde diese auf eigene Kosten rechtzeitig durchführen.<br />
<br />
(3) Bei Pfändungen oder sonstigen Eingriffen Dritter hat uns der Kunde unverzüglich schriftlich zu benachrichtigen, damit wir Klage gemäß § 771 ZPO erheben können. Soweit der Dritte nicht in der Lage ist, uns die gerichtlichen und außergerichtlichen Kosten einer Klage gemäß § 771 ZPO zu erstatten, haftet der Kunde für den uns entstandenen Ausfall.<br />
<br />
(4) Der Kunde ist berechtigt, die Kaufsache im ordentlichen Geschäftsgang weiter zu verkaufen; er tritt uns jedoch bereits jetzt alle Forderungen in Höhe des Faktura-Endbetrages (einschließlich MwSt) unserer Forderung ab, die ihm aus der Weiterveräußerung gegen seine Abnehmer oder Dritte erwachsen, und zwar unabhängig davon, ob die Kaufsache ohne oder nach Verarbeitung weiterverkauft worden ist. Sollte zwischen dem Kunden und dem Abnehmer ein Kontokorrentverhältnis nach § 355 HGB bestehen, bezieht sich die uns vom Kunden im Voraus abgetretene Forderung auch auf den anerkannten Saldo sowie im Fall der Insolvenz des Abnehmers auf den dann vorhandenen kausalen Saldo. Zur Einziehung dieser Forderung bleibt der Kunde auch nach der Abtretung ermächtigt. Unsere Befugnis, die Forderung selbst einzuziehen, bleibt hiervon unberührt. Wir verpflichten uns jedoch, die Forderung nicht einzuziehen, solange der Kunde seinen Zahlungsverpflichtungen aus den vereinnahmten Erlösen nachkommt, nicht in Zahlungsverzug gerät und insbesondere kein Antrag auf Eröffnung eines Vergleichs- oder Insolvenzverfahrens gestellt ist oder Zahlungseinstellung vor-liegt. Ist aber dies der Fall, so können wir verlangen, dass der Kunde uns die abgetretenen Forderungen und deren Schuldner bekannt gibt, alle zum Einzug erforderlichen Angaben macht, die dazugehörigen Unterlagen aushändigt und den Schuldnern (Dritten) die Abtretung mitteilt.<br />
<br />
(5) Die Verarbeitung oder Umbildung der Kaufsache durch den Kunden wird stets für uns vorgenommen. Wird die Kaufsache mit anderen, uns nicht gehörenden Gegenständen verarbeitet, so erwerben wir das Miteigentum an der neuen Sache im Verhältnis des Wertes der Kaufsache (Faktura-Endbetrag, einschließlich MwSt) zu den anderen verarbeiteten Gegenständen zur Zeit der Verarbeitung. Für die durch Verarbeitung entstehende Sache gilt im Übrigen das Gleiche wie für die unter Vorbehalt gelieferte Kaufsache.<br />
<br />
(6) Wird die Kaufsache mit anderen, uns nicht gehörenden Gegenständen untrennbar vermischt, so erwerben wir das Miteigentum an der neuen Sache im Verhältnis des Wertes der Kaufsache (Faktura-Endbetrag, einschließlich MwSt) zu den anderen vermischten Gegenständen zum Zeitpunkt der Vermischung. Erfolgt die Vermischung in der Weise, dass die Sache des Kunden als Hauptsache anzusehen ist, so gilt als vereinbart, dass der Kunde uns anteilmäßig Miteigentum überträgt. Der Kunde verwahrt das so entstandene Alleineigentum oder Miteigentum für uns.<br />
<br />
(7) Der Kunde tritt uns auch die Forderungen zur Sicherung unserer Forderungen gegen ihn ab, die durch die Verbindung der Kaufsache mit einem Grundstück gegen einen Dritten erwachsen.<br />
<br />
(8) Wir verpflichten uns, die uns zustehenden Sicherheiten auf Verlangen des Kunden insoweit freizugeben, als der realisierbare Wert unserer Sicherheiten die zu sichernden Forderungen um mehr als 10 % übersteigt; die Auswahl der freizugebenden Sicherheiten obliegt uns.</p>
<h2>7. Mängelrüge</h2>
<p>Mängelansprüche des Kunden setzen voraus, dass dieser seinen nach § 377 HGB geschuldeten Untersuchungs- und Rügeobliegenheiten ordnungsgemäß nachgekommen ist.</p>
<h2>8. Gewährleistung und Haftung</h2>
<p>8.1. Die Beschaffenheit der Ware richtet sich ausschließlich nach den vereinbarten technischen Liefervorschriften. Falls wir nach Zeichnungen, Spezifikationen, Mustern usw. eines Partners zu liefern haben, übernimmt dieser das Risiko der Eignung für den vorgesehenen Verwendungszweck. Entscheidend für den vertragsgemäßen Zustand der Ware ist der Zeitpunkt des Gefahrenübergangs gemäß Ziffer 5.1. Ergänzend wird für Textilien auf die a) „Richtlinie zur Beurteilung von konfektionierten Markisentüchern“, Stand Dezember 2016, des ITRS (Industrieverband Technische Textilien – Rollladen- Sonnenschutz e.V.) hingewiesen, sowie b) die „Richtlinie zur Reinigung und Pflege von Markisentüchern“, Stand Juni 2010 des ITRS (Industrieverband Technische Textilien – Rollladen- Sonnenschutz e.V.) verwiesen.<br />
Bei Produktbestandteilen aus Aluminium gilt die „Richtlinie zur Beurteilung der Produkteigenschaften von Markisen“, Stand Februar 2018 des ITRS (Industrieverband Technische Textilien – Rollladen- Sonnenschutz e.V.), die ebenso Gegenstand unserer Vertragsbeziehung sind. Diese Richtlinien sind gratis abrufbar über https://itrs-ev.com  Publikationen.<br />
<br />
8.2. Soweit ein Mangel der Kaufsache vorliegt, sind wir nach unserer Wahl zur Nacherfüllung in Form einer Mangelbeseitigung oder zur Lieferung einer neuen mangelfreien Sache berechtigt. Im Fall der Nacherfüllung tragen wir die erforderlichen Aufwendungen nur bis zur Höhe des Kaufpreises.<br />
<br />
8.3. Schlägt die Nacherfüllung fehl, so ist der Kunde nach seiner Wahl berechtigt, Rücktritt oder Minderung zu verlangen.<br />
<br />
8.4. Wir haften nach den gesetzlichen Bestimmungen, sofern der Kunde Schadensersatzansprüche geltend macht, die auf Vorsatz oder grober Fahrlässigkeit, einschließlich von Vorsatz oder grober Fahrlässigkeit unserer Vertreter oder Erfüllungsgehilfen beruhen. Soweit uns keine vorsätzliche Vertragsverletzung angelastet wird, ist die Schadensersatzhaftung auf den vorhersehbaren, typischerweise eintretenden Schaden begrenzt.<br />
<br />
8.5. Wir haften nach den gesetzlichen Bestimmungen, sofern wir schuldhaft eine wesentliche Vertragspflicht verletzen; auch in diesem Fall ist aber die Schadensersatzhaftung auf den vorhersehbaren, typischerweise eintretenden Schaden begrenzt.<br />
<br />
8.6. Soweit dem Kunden im Übrigen wegen einer fahrlässigen Pflichtverletzung ein Anspruch auf Ersatz des Schadens statt der Leistung zusteht, ist unsere Haftung auf Ersatz des vorhersehbaren, typischerweise eintretenden Schadens begrenzt.<br />
<br />
8.7. Die Haftung wegen schuldhafter Verletzung des Lebens, des Körpers oder der Gesundheit bleibt unberührt; dies gilt auch für die zwingende Haftung nach dem Produkthaftungsgesetz.<br />
<br />
8.8. Soweit nicht vorstehend etwas Abweichendes geregelt ist, ist die Haftung aus-geschlossen.<br />
<br />
8.9. Die Verjährungsfrist für Mängelansprüche beträgt 24 Monate, gerechnet ab Gefahrenübergang. Dies gilt nicht, soweit die Kaufsache üblicherweise für ein Bauwerk verwendet wird und den Mangel verursacht hat.<br />
<br />
8.10. Die Verjährungsfrist im Fall eines Lieferregresses nach den §§ 478, 479 BGB bleibt unberührt; sie beträgt fünf Jahre, gerechnet ab Ablieferung der mangelhaften Sache.</p>
<h2>9. Haftung für Nebenpflichten, sonstige Haftung</h2>
<p>9.1. Eine weitergehende Haftung auf Schadensersatz als in Abschnitt 8 vorgesehen, ist – ohne Rücksicht auf die Rechtsnatur des geltend gemachten Anspruchs – ausgeschlossen. Dies gilt insbesondere für Schadensersatzansprüche aus Verschulden bei Vertragsabschluss, wegen sonstiger Pflichtverletzungen oder wegen deliktischer Ansprüche auf Ersatz von Sachschäden gemäß § 823 BGB.<br />
<br />
9.2. Die Begrenzung nach Abschnitt 9.1. gilt auch, soweit der Kunde anstelle eines Anspruchs auf Ersatz des Schadens, statt der Leistung Ersatz nutzloser Aufwendungen verlangt.<br />
<br />
9.3. Soweit die Schadensersatzhaftung uns gegenüber ausgeschlossen oder eingeschränkt ist, gilt dies auch im Hinblick auf die persönliche Schadensersatzhaftung unserer Angestellten, Arbeitnehmer, Mitarbeiter, Vertreter und Erfüllungsgehilfen.</p>
<h2>10. Schutzrecht, Werkzeuge</h2>
<p>10.1. Der Besteller übernimmt für die von ihm beizubringenden Unterlagen, Pläne, Muster oder dergleichen die alleinige Verantwortung. Der Besteller hat dafür einzustehen, dass von ihm vorgelegte Ausführungszeichnungen in Schutzrechte Dritter nicht eingreifen. Wir sind dem Besteller gegenüber nicht zur Prüfung verpflichtet, ob durch Abgabe von Angeboten irgendwelche Schutzrechte Dritter verletzt werden. Ergibt sich trotzdem eine Haftung, so hat der Besteller uns schadenlos zu halten.<br />
Wir haften nach den gesetzlichen Bestimmungen, sofern der Kunde Schadensersatzansprüche geltend macht, die auf Vorsatz oder grober Fahrlässigkeit, einschließlich von Vorsatz oder grober Fahrlässigkeit unserer Vertreter oder Erfüllungsgehilfen beruhen. Soweit uns keine vorsätzliche Vertragsverletzung angelastet wird, ist die Schadensersatzhaftung auf den vorhersehbaren, typischerweise eintretenden Schaden begrenzt.<br />
10.2. Die von uns zur Herstellung der Vertragsgegenstände im Auftrag des Bestellers hergestellten Betriebsgegenstände, insbesondere Werkzeuge, Vorrichtungen usw. bleiben auch dann, wenn sie gesondert berechnet werden oder der Besteller sich an deren Kosten beteiligt hat, unser Eigentum und werden auch bei Vertragsbeendigung nicht ausgeliefert.</p>
<h2>11. Erfüllungsort und Gerichtsstand</h2>
<p>11.1. Sofern sich aus der Auftragsbestätigung nichts anderes ergibt, ist unser<br />
Geschäftssitz – also der Sitz unserer Firma in Betzenweiler – Erfüllungsort.<br />
<br />
11.2. Sofern der Kunde Kaufmann ist, ist unser Geschäftssitz Gerichtsstand; wir sind jedoch berechtigt, den Kunden auch an seinem Wohnsitzgericht zu verklagen.<br />
<br />
11.3. Es gilt das Recht der Bundesrepublik Deutschland. Die Bestimmungen des UN-Kaufrechts sind ausdrücklich ausgeschlossen.</p>
<h2>12. Änderungsvorbehalt</h2>
<p>Wir erklären dem Besteller ausdrücklich unsere Bereitschaft, im Wege des freien gegenseitigen Aushandelns die Vertragsklauseln inhaltlich auszugestalten.</p>
