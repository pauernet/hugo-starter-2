let tailwindConfig =
  process.env.HUGO_FILE_TAILWIND_CONFIG_JS || "./tailwind.config.js";
const tailwind = require("tailwindcss")(tailwindConfig);
const autoprefixer = require("autoprefixer");

module.exports = {
  // eslint-disable-next-line no-process-env
  plugins: [
    tailwind,
    ...(process.env.HUGO_ENVIRONMENT === "production" ? [autoprefixer] : []),
  ],
};
// const themeDir = __dirname + "/../../";

// module.exports = {
//   plugins: [
//     require("postcss-import")({
//       path: [themeDir],
//     }),
//     require("tailwindcss")(themeDir + "assets/css/tailwind.config.js"),
//     require("autoprefixer")({
//       path: [themeDir],
//     }),
//   ],
// };

// const purgecss = require("@fullhuman/postcss-purgecss")({
//   content: ["./hugo_stats.json"],
//   defaultExtractor: (content) => {
//     let els = JSON.parse(content).htmlElements;
//     return els.tags.concat(els.classes, els.ids);
//   },
// });

// module.exports = {
//   plugins: [
//     require("tailwindcss"),
//     require("autoprefixer"),
//     ...(process.env.HUGO_ENVIRONMENT === "production" ? [purgecss] : []),
//   ],
// };
