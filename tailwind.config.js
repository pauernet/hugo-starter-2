const theme = require("tailwindcss/defaultTheme");
const typography = require("@tailwindcss/typography");

//const colorBrand = 'var(--color-pretty)';

// Utils
const round = (num) =>
  num
    .toFixed(7)
    .replace(/(\.[0-9]+?)0+$/, "$1")
    .replace(/\.0$/, "");
const rem = (px) => `${round(px / 16)}rem`;
const em = (px, base) => `${round(px / base)}em`;
const px = (px) => `${px}px`;

module.exports = {
  important: true, // See https://tailwindcss.com/docs/configuration#important
  purge: {
    enabled: process.env.HUGO_ENVIRONMENT === "production",
    content: ["./hugo_stats.json", "./layouts/**/*.html"],
    extractors: [
      {
        extractor: (content) => {
          let els = JSON.parse(content).htmlElements;
          return els.tags.concat(els.classes, els.ids);
        },
        extensions: ["json"],
      },
    ],
    mode: "all",
    options: {
      safelist: [],
    },
  },
  theme: {
    fontFamily: {
      sans: ["Roboto", "sans-serif"],
    },
    zIndex: {
      0: 0,
      5: 5,
      10: 10,
      15: 15,
      20: 20,
      25: 25,
      30: 30,
      35: 35,
      40: 40,
      45: 45,
      50: 50,
      55: 55,
      60: 60,
      65: 65,
      70: 70,
      75: 75,
      80: 80,
      85: 85,
      90: 90,
      95: 95,
      105: 105,
      auto: "auto",
    },
    extend: {
      colors: {
        "grey-dark": "hsla(0, 0%, 47%, 1)",
        "grey-light": "hsla(0, 0%, 76%, 1)",
        "grey-lightest": "hsla(0, 0%, 94%, 1)",
      },
      width: {
        text: "80ch",
        sm: "640px",
        md: "768px",
        lg: "1024px",
        xl: "1280px",
        xxl: "1536px",
        fit: "fit-content",
      },
      maxWidth: {
        text: "80ch",
        max: "1356px",
        vw: "100vw",
        10: "10%",
        20: "20%",
        30: "30%",
        40: "40%",
        50: "50%",
        60: "60%",
        70: "70%",
        80: "80%",
        90: "90%",
        sm: "640px",
        md: "768px",
        lg: "1024px",
        xl: "1280px",
        xxl: "1536px",
      },
      height: {
        min: "min-content",
        max: "max-content",
      },
    },
  },
  variants: {
    extend: {
      zIndex: ["hover", "active"],
    },
  },
  plugins: [typography],
};
